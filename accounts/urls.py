from django.urls import path
from accounts.views import user_login, user_logout, signup_user

urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup_user, name="signup")
]
